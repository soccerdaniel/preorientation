##########################################################################
# Explore dataset
# Remark: Headturn per seconds are shit!
##########################################################################

# Load packages and setup workspace ---------------------------------------
rm(list=ls()) # Clears environment variables
cat("\014")  # Clears console commands

# Getting the path of your current open file
current_path = rstudioapi::getActiveDocumentContext()$path 
setwd(dirname(current_path ))
setwd('..') # Moves up one folder. 
getwd() # getwd() should be the repository folder '.\preorientation'

if(!require(rethinking)) {
  install.packages("lavaan"); require(rethinking)} #load / install+load 

if(!require(dagitty)) {
  install.packages("dagitty"); require(dagitty)} #load / install+load 

if(!require(dplyr)) {
  install.packages("dplyr"); require(dplyr)} #load / install+load 

if(!require(bnlearn)) {
  install.packages("dplyr"); require(bnlearn)} #load / install+load 



# Load experimental variables ---------------------------------------------
df <- (read.delim("./02_Data/AggregatedData.txt", row.names = 1))
colnames(df)
df <- df[-c(1),]

# Center predictors and outcome
df$DurationHeadTurned.c <- df$DurationHeadTurned - mean(df$DurationHeadTurned)
df$NumHeadTurns.c <- df$NumHeadTurns - mean(df$NumHeadTurns)
df$CorrectDecisions.c <- df$CorrectDecisions - mean(df$CorrectDecisions)
df$FwdOrientation.c <- df$FwdOrientation - mean(df$FwdOrientation)
df$ExpertiseIndex.c <- df$ExpertiseIndex - mean(df$ExpertiseIndex)
df$HeadTurnsPerSecond.c <- df$HeadTurnsPerSecond - mean(df$HeadTurnsPerSecond)
df$HeadShoulderTurnsPerSecond.c <- df$HeadShoulderTurnsPerSecond - mean(df$HeadShoulderTurnsPerSecond)


# Scale predictor and outcome
df$DurationHeadTurned.z <- scale(df$DurationHeadTurned)
df$NumHeadTurns.z <- scale(df$NumHeadTurns)
df$FwdOrientation.z <- scale(df$FwdOrientation)
df$TimeShouldersNeutral.z <- scale(df$TimeShouldersNeutral)
df$ExpertiseIndex.z <- scale(df$ExpertiseIndex)
df$HeadTurnsPerSecond.z <- scale(df$HeadTurnsPerSecond)
df$HeadShoulderTurnsPerSecond.z <- scale(df$HeadShoulderTurnsPerSecond)




# Exploit dependence structure --------------------------------------------
library(dplyr)
dfNumeric <- select_if(df, is.numeric)

cor(dfNumeric, df$ExpertiseIndex)
cor(dfNumeric, df$NumHeadTurns)
# Head turns are not related to expertise, but to Age. old players dont turn that much
# NumHead is basically the same like NumHeadShoulders. Meaning that the more the head was turned, the more shoulders were also turned
# HeadTurns is positively related to forward orientation. The more head turns, the more the orientation was forward.

cor(dfNumeric, df$CorrectDecisions)
# Correct decisions are made by experts
# Headturns are bad

cor(dfNumeric, df$DominantLegIndex)
# Left footies are really bad! (have index 1)

cor(dfNumeric, df$FwdOrientation)
# Experts are more forward oriented
# players who are more forward oriented, turn the head more (but experts dont!)

cor(dfNumeric, df$HeadShoulderTurnsPerSecond) # Experts use it more and correctness correlates with it

cor(dfNumeric, df$HeadTurnsPerSecond) # Experts dont use it, yet its a good measure for correct decisions

cor(dfNumeric, df$DurationHeadTurned) # Experts dont have their head turn long. yet its a good way of becoming better



# Lets see whether experts use orientation for success --------------------
lm(df$CorrectDecisions ~ df$ExpertiseIndex.z) # Expertise helps
lm(df$CorrectDecisions ~df$FwdOrientation.z) # Orientation no effect
lm(df$CorrectDecisions ~df$ExpertiseIndex.z + df$FwdOrientation.z) # What is this masked effect
### Its when two correlated predictors are used. one has a positive influence on the outcome, the other one a negative!!
## Possible interpretaion: Orientation only helps you wehen you are an expert
lm(df$CorrectDecisions ~df$ExpertiseIndex.z + df$FwdOrientation.z + df$ExpertiseIndex.z:df$FwdOrientation.z) # Masking is even more prominent

  # Get the masking explained
  cor(df$CorrectDecisions, df$FwdOrientation) # negative correlation with outcome
  cor(df$CorrectDecisions, df$ExpertiseIndex) # positive correlation with outcome
  cor(df$ExpertiseIndex, df$FwdOrientation) # Positive correlation between the predictors!
    ## Elreath140:  # Players with good decisions, for their expertise level, do they have high orientation?
                    # Players with good decisions, for their orientation, do they have high expertise?
  
    # Simulate Masked relationship
    N <- 100
    rho <- 0.56 # Correlation between epxertise and fwdOrientation
    x_pos <- rnorm(N) # Expertise
    x_neg <- rnorm(N, rho*x_pos, sqrt(1-rho^2)) # Orientation is positively associated with Expertise. If rho is close to 1 or -1, there is multicollinearity
    y <- rnorm(N, x_pos - x_neg) # Correctness is equally correlated with both predictors
    # Predictors are correlated
    plot(x_pos,x_neg)    
    cor(x_pos,x_neg) 
    lm(x_pos~x_neg)
    # Outcome is correlated
    cor(y,x_neg)
    cor(y,x_pos)
    df.sim <- data.frame(y,x_pos,x_neg)
    # Compute regressions
    lm(y ~ x_pos) 
    lm(y ~ x_neg)
    lm(y ~ x_pos + x_neg) #Indeed, both predictors together explain more


# LectureNotes Model ------------------------------------------------------
# Assumption
    lm(df$CorrectDecisions ~ df$ExpertiseIndex.z)
    lm(df$CorrectDecisions ~ df$HeadShoulderTurnsPerSecond.z + df$ExpertiseIndex.z) # Shows that controlling for entire turns, expertise has a smaller influence
    lm(df$CorrectDecisions ~ df$HeadShoulderTurnsPerSecond.z + df$ExpertiseIndex.z) # Shows that controlling for entire turns, expertise has a smaller influence
    
    lm(df$CorrectDecisions ~ df$FwdOrientation.z)
    lm(df$CorrectDecisions ~ df$HeadShoulderTurnsPerSecond.z)
    lm(df$CorrectDecisions ~ df$HeadShoulderTurnsPerSecond.z + df$FwdOrientation.z) # Orientation is good for all those who are turned entirely more often
    lm(df$CorrectDecisions ~ df$HeadShoulderTurnsPerSecond.z + df$FwdOrientation.z + df$ExpertiseIndex.z) # This effect vanishes again, when controlling for expertise!
      # Learning: Experts turn entirely more often. this changes their orientation 
    
    
    
    lm(df$CorrectDecisions ~ df$DurationHeadTurned.z + df$ExpertiseIndex.z)
    
    lm(df$CorrectDecisions ~ df$HeadTurnsPerSecond.z + df$ExpertiseIndex.z)
    
    
# Lets see whether experts use head turns for success ---------------------
lm(df$CorrectDecisions ~ df$ExpertiseIndex.z) # Expertise helps
lm(df$CorrectDecisions ~ df$NumHeadTurns.z) # Head turns dont help (-3)
lm(df$CorrectDecisions ~df$ExpertiseIndex.z + df$NumHeadTurns.z) # Nothing happens.
## Expertise is independent of number of head turns!


lm(df$CorrectDecisions ~ df$NumHeadTurns.z)
lm(df$CorrectDecisions ~ df$DurationHeadTurned.z)
lm(df$CorrectDecisions ~ df$DurationHeadTurned.z + df$NumHeadTurns.z)

lm(df$CorrectDecisions ~ df$NumHeadTurns.z + df$FwdOrientation.z)
lm(df$CorrectDecisions ~ df$FwdOrientation.z)
lm(df$CorrectDecisions ~ df$NumHeadTurns.z)


lm(df$CorrectDecisions ~ df$DurationHeadTurned.z + df$FwdOrientation.z)
lm(df$CorrectDecisions ~ df$FwdOrientation.z)
lm(df$CorrectDecisions ~ df$DurationHeadTurned.z)
  ## Head turns cause fwd orientation. Fwd orientation is actually good, where as the headturns themselves arent!!!!!



# Relationship Orientation and Num Head -----------------------------------
# Check subgroups of head turns to make sure
dfManyHeadTurns <- df[which(df$NumHeadTurns.z > 0),]
dfFewHeadTurns <- df[which(df$NumHeadTurns.z < 0),]
lm(dfManyHeadTurns$CorrectDecisions ~ dfManyHeadTurns$FwdOrientation.z)
lm(dfFewHeadTurns$CorrectDecisions ~ dfFewHeadTurns$FwdOrientation.z)
## Conclusion: People who dont turn the head often, benefit from being oriented forwards!
# People who turn the head often, have a bad performance, if they additionally face forwards.

# Head turns cause fwd orientation. Fwd orientation is actually good, where as the headturns themselves arent!!!!!
dfBigOrientation <- df[which(df$FwdOrientation.z > 0),]
dfLittleOrientation <- df[which(df$FwdOrientation.z < 0),]
lm(dfBigOrientation$CorrectDecisions ~ dfBigOrientation$NumHeadTurns.z)
lm(dfLittleOrientation$CorrectDecisions ~ dfLittleOrientation$NumHeadTurns.z)
## Conclusion: For people who are already turned forward, additional head turns are bad. For people who arent turned forward, turning the head has no effect


# Relationship Orientation and Duration -----------------------------------


plot(df$DurationHeadTurned.z, df$FwdOrientation.z)
plot(df$DurationHeadTurned.z, df$NumHeadTurns.z)
cor(df$NumHeadTurns.z, df$FwdOrientation.z)
# head turns cause more fwd orientation
# duration head turned causes less orientation






# Check subgroups of head turns to make sure
dfLongDuration <- df[which(df$DurationHeadTurned.z > 0),]
dfShortDuration <- df[which(df$DurationHeadTurned.z < 0),]
lm(dfLongDuration$CorrectDecisions ~ dfLongDuration$FwdOrientation.z)
lm(dfShortDuration$CorrectDecisions ~ dfShortDuration$FwdOrientation.z)
## Conclusion: People who dont turn the head often, benefit from being oriented forwards!
# People who turn the head often, have a bad performance, if they additionally face forwards.

# Disregarding the orientation, players who have their head turned forwards for more time, are better
dfBigOrientation <- df[which(df$FwdOrientation.z > 0),]
dfLittleOrientation <- df[which(df$FwdOrientation.z < 0),]
lm(dfBigOrientation$CorrectDecisions ~ dfBigOrientation$DurationHeadTurned.z)
lm(dfLittleOrientation$CorrectDecisions ~ dfLittleOrientation$DurationHeadTurned.z)
## Conclusion: For people who are already turned forward, additional head turns are bad. For people who arent turned forward, turning the head has no effect




# Start over --------------------------------------------------------------
# New ideas ---------------------------------------------------------------
lm(df$CorrectDecisions ~ df$DurationHeadTurned.z)
lm(df$CorrectDecisions ~ df$NumHeadTurns.z)

lm(df$NumHeadTurns ~ df$ExpertiseIndex.c)
lm(df$DurationHeadTurned ~ df$ExpertiseIndex.c)
plot(df$DurationHeadTurned,df$ExpertiseIndex)



