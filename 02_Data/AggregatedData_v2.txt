VPN	Age	DominantLeg	DominantLegIndex	DeliberatePlay	DeliberatePractice	HighestDivision	ExpertiseIndex	Position	PosIndex	CorrectDecisions	NumHeadTurns	HeadTurnFrequency	DurationOfATurn	NumTurnsLeft	NumTurnsRight	NumHeadShoulders	BodyTurnFrequency	DurationHeadShoulderTurned	TimeLeftShoulderTurned	TimeShouldersNeutral	TimeRightShoulderTurned	FwdOrientation
1	20	re	0	12	12	1. Liga classic	3	Mittelfeld defensiv	2	29.06	4.07	2.49	0.51	2.42	1.65	3.38	2.54	1.12	34.13	47.09	18.78	52.91
2	21	re	0	16	16	1. Liga classic/Nati LI	3	Mittelfeld defensiv	2	89.66	4.82	1.73	0.72	3.5	1.34	4.35	1.93	0.95	41.32	37.26	21.42	62.74
3	22	re	0	15	9	3. Liga	1	Innerverteidiger	1	86.55	4.11	2.26	0.8	2.08	2.03	3.73	2.43	0.94	29.53	43.25	27.22	56.75
4	24	re	0	11	11	4. Liga	0	Aussenverteidiger re	1	81.67	3.11	2.99	0.9	1.44	1.62	3.22	2.68	1.23	20.84	72.17	6.99	27.83
5	23	li	1	15	15	3. Liga	1	Fl?el li	3	68.91	4.01	2.17	0.57	1.53	2.54	4.39	1.83	0.77	25.07	47.34	27.59	52.66
6	20	re	0	13	13	3. Liga	1	Sturm	3	91.6	3.67	2.34	0.79	2.44	1.16	3.27	2.47	0.92	21.07	43.06	35.87	56.94
7	20	re	0	15	15	1. Liga classic	3	Torh?er	0	81.51	4.24	1.93	0.74	2.52	1.71	3.46	2.24	1.33	22.39	36.11	41.5	63.89
8	25	re	0	12	12	3. Liga	1	Verteidigung (alles)	1	77.97	3.32	3.14	0.61	1.25	1.99	3.24	2.77	0.82	25.6	58.57	15.83	41.43
9	22	re	0	16	16	2. Liga regional	2	Mittelfeld offensiv	2	91.67	4.59	2.01	0.66	2.26	2.27	4.06	2.1	0.86	23.57	39.29	37.15	60.72
10	20	re	0	10	5	U-15 	0	Verteidigung li	1	79.17	5.37	1.42	0.77	3.76	1.79	5.5	1.43	0.85	20.22	50.18	29.6	49.82
11	22	re	0	17	17	2. Liga interregional	4	Mittelfeld defensiv	2	92.5	3.52	2.88	0.65	1.66	1.63	2.74	3.2	1.28	11.23	37.29	51.48	62.71
12	21	re	0	10	10	3. Liga	1	Aussenverteidiger re/li	1	72.41	5.82	1.34	0.59	3.93	1.84	4.24	1.83	1.32	64.48	25.63	9.9	74.38
13	20	li	1	14	13	2. Liga regional	2	Mittelfeld defensiv	2	69.83	5.59	1.52	0.71	3.03	2.64	4.69	1.65	1.09	36.2	29.66	34.15	70.35
