



% Main document
% =====================================

% For actualized citations:
%	Include a .bib library (e.g. from mendeley or similar)
% Compile the PDF first als "pdfLaTeX", then as "BibTex" and then twice as "pdfLaTeX" to make references and citations visible (creates a .bbl file)

%---------------------------------------------------------------------------
\documentclass[
	a4paper,								% paper format
	12pt,									% fontsize
	twoside								% double-sided
]{article}


% Load Standard Packages:
%---------------------------------------------------------------------------
\usepackage[ngerman,english]{babel}					% German and english grammar
\usepackage{blindtext}							% Input dummy text
\usepackage{setspace}							% To set space between lines to 1.2 (using setstretch command)
\usepackage{geometry}							% To set up page geometry
\usepackage{xspace}							% After , and . spacing is adjusted automatically
\usepackage[utf8]{inputenc}						% To add Umlaute äöü
\usepackage{fancyhdr}							% For headers and footers
\usepackage{graphicx}							% For images
\usepackage{float} 							% Float before pictures
\usepackage{wrapfig} 							% To wrap text around picture
\usepackage[hidelinks]{hyperref}					% For inclusion of url references // Has to come before apacite package
\usepackage{apacite}							% For APA-style citations
\usepackage{float}								% floating images
\usepackage{caption}							% for captions of figures and tables
\usepackage{multirow}							% For tables
\usepackage{booktabs}							% For figures and tables
\usepackage{tikz}								% For figures and tables
\usepackage{enumitem}							% For Text enumerated list {description}
\usepackage{listings}							% For codeblocks
\usepackage{xcolor}							% For colors
\usepackage{mdframed}							% For framed code blocks in blue
\usepackage{program}							% For pseudocode blocks

\usepackage[linesnumbered,lined,commentsnumbered]{algorithm2e}	% For algorithmic display

\usepackage{lineno}							% Displaying line numbers for review
%\linenumbers


% Page Setup	
%---------------------------------------------------------------------------	
\geometry{
	a4paper,
	inner=20mm,
	outer=25mm,
	top=25mm,
	headheight=20mm,
	headsep=10mm,
	textheight=242mm,
	footskip=15mm
}	

\setstretch{1}								% Set line spacing
\pagestyle{fancy}								% To make header and footer, child of fancyhdr package
\fancyhf{}
\fancyhead[LE,RO]{Daniel Müller}					% L or R for left or right pages, E or O for even or odd pages, c for centered
%\fancyhead[RE,LO]{\leftmark}
\fancyfoot[LE,RO]{\thepage}
\raggedbottom								% Saggy outflow on bottom of page
\author{Daniel Müller}							% Set the author of the document
\date{\today}								% Set the date as today
\setlength\parindent{0pt}							% Set indent to 0 for entire document
\graphicspath{{01_Graphics/}} 						% Setting the graphicspath
	
\mdfdefinestyle{Codeframe}{%						% Define a codeframe box with blue background
    linecolor=white,
    outerlinewidth=2pt,
    %roundcorner=20pt,
    innertopmargin=6pt,
    innerbottommargin=6pt,
    innerrightmargin=6pt,
    innerleftmargin=6pt,
        leftmargin = 6pt,
        rightmargin = 6pt,
    backgroundcolor=blue!5!white
}
\captionsetup[table]{textfont=it,format=plain,justification=justified, 
  singlelinecheck=false,skip=0pt}						% Setup for tables


%Title and Tables of Contents
%---------------------------------------------------------------------------
\begin{document}


% Titlepage
%---------------------------------------------------------------------------

\begin{titlepage}
\setcounter{page}{0}	 % Titlepage has pagenumber 0
	\centering
	{\huge\bfseries Preorientation\par}
	\vspace{1 cm}
	{\LARGE\bfseries Experimental paradigm of visual exploratory behavior in soccer\par}
	\vspace{6cm}
	{\normalsize Repository of \today \par}
	 \href{https://bitbucket.org/soccerdaniel/preorientation/src}{bitbucket.org/soccerdaniel/preorientation} \par
	\vspace{4 cm}
	\includegraphics[scale = 0.5]{01_Graphics/thumbnail.png}	
	\vfill	% Putting the rest at the bottom of the page
	 \url{http://www.science.football}
\end{titlepage}

%TOC
%---------------------------------------------------------------------------
\setcounter{tocdepth}{3}							% Sets the table of content to show only level 1 entries
\tableofcontents
\newpage


% Content
%---------------------------------------------------------------------------
\section{Data}
Experimental Data of 12 soccer players was evaluated for the experiment. The treatment assessed various decision making variables for analysis. The following treatment variables are included in the dataset:
\medskip
\begin{description}[itemsep = 0 pt, leftmargin = 0 cm, nolistsep]
\item [Expertise:] Assessed level of expertise approximated by highest playing division
\item [Correct Decisions:] Aggregated percentage of correct decisions in all 120 trials
\item [Head turns per second:] Number of times the head was turned more than 45 degrees relative to the shoulder position per second
\item [Head and shoulder turns per second:] Inverse of Number of turns head and shoulder
\item [Duration of a Turn:] Average time in seconds, indicating how long the head remained turned
\item [Forward Orientation:] Time in seconds player facing away from the ball looking in playing direction
\end{description}


\newpage
\section{Causal model}
Following the tutorial of \citeA{Tennant2017}, the causal structure of observational variables can be modeled along a temporal dimension. As for this manual, the relationship between the number of head turns and its influence on the correctnes of a decision with the following pass is assessed with the following DAG:
\begin{figure}[H]
\begin{center}
\includegraphics[trim = 0 200 0 200, clip = true, width = 10cm]{dag0.png}
\caption{DAG with the influence of interest}
\label{fig:SimpleDag}
\end{center}
\end{figure}
The DAG assumes that the correction of the Decision is caused by the number of head turns, the player makes. This model is too simple, because ignores that the better the ball control of a player is, the less it interferes with the task solution. Expert players could simply perform better in this test, because the motor response is simpler to perform. Expertise is thus a confounder of HeadTurnFrequency.

\begin{figure}[H]
\begin{center}
\includegraphics[trim = 0 0 0 0, clip = true, width = 10cm]{dag1.png}
\caption{DAG with different expert strategies}
\label{fig:ConfoundedDag}
\end{center}
\end{figure}

Beneath the possible confound of expertise on HeadTurnFrequency, experts could also use other strategies to get a decision right. Since the task is to detect a free player on the pitch, turning the head more often might not help solemnly to find the free player. in figure \ref{fig:ConfoundedDag}, two other ways to get the task right are assumed: First, Experts can vary the duration of each turn. Maybe they need less turns but each individual turn has a longer duration towards the pitch. Second, Experts could use their body orientation - instead of turning only the head, turning the head and the entire body. This facilitates the motor response: Playing the ball to the right player can be performed with more ease.


\section{Simulated Data}
Under the assumption that the presented DAG in figure \ref{fig:ConfoundedDag} is correct, the effect of each mediator (DurationOfATurn, HeadTurnFrequency, BodyTurnFrequency) can be read directly from the equation below. Controlling for expertise is necessary since it is a common confounder of all mediators: \\

$ CorrectDecisions_i \sim Normal(\mu_1, \sigma)$ \\
$ \mu_1 = \alpha + \beta_i Mediator_j + \beta_i Expertise$\\

This equation holds only in case each mediator has an effect on expertise, independent of the other two mediators:\\
$DurationOfATurn \perp\!\!\!\perp HeadTurnFrequency \perp\!\!\!\perp BodyTurnFrequency$ \\

Assessing whether expertise is exclusively influencing the mediators can be checked by looking at the influence directly: \\
$ Mediator_i \sim Normal(\mu_1, \sigma)$ \\
$ \mu_1 = \alpha + \beta_i Expertise$\\


\section{Preparing predictors}
Since the main goal is to get the estimates of the mediators (DurationOfATurn, HeadTurnFrequency, BodyTurnFrequency), these predictors will be centered and scaled (Using z-scores as in  \citeA[p.~56]{Gelman2007}). The same is done for Expertise, as it needs to be included as a possible confound.













%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%OBSOLETE BUILDING BLOCKS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%
%% Building Blocks
%\newpage %----------------------------------------------------------------
%% CODEFRAME BLOCK
%\begin{mdframed}[style=Codeframe]
%\textbf{THIS TEXT SHOULD BE MORE TIGHTLY FRAMED} \par
%Now flowtext
%\end{mdframed}
%
%% CITATIONS
%\cite{Elwert2013}  \\					% Full citation
%\citeNP{Elwert2013} \\					% Full citation, no parenthesis
%\cite[p.~12]{Elwert2013} \\				% Full citation with page reference
%\citeA<e.g.,>[p.~11]{Elwert2013} \\ 			% Full citation of the author outside the parenthesis
%\citeyear<e.g.,>[p.~11]{Elwert2013} \\		% Ciitation of the year and page only in the parenthesis
%\citeyearNP<e.g.,>[p.~11]{Elwert2013} \\		% Citation of the year and page only, no parenthesis
%
%
%% CODEBLOCK
%\begin{figure}[H]
%\begin{center}
%\includegraphics[trim = 0 0 0 0, clip = true, width = 12cm]{thumbnail.png}
%\caption{A picture}
%\label{fig:ALabel}
%\end{center}
%\end{figure}
%
%% CODEBLOCK DESCRIPTION
%\begin{description}[itemsep = 0 pt, nolistsep, leftmargin = 0 cm]
%\item [Age:] Participants age
%\item [Position:] Preferred position 
%\end{description}
%% Hierarchy
%%---------------------------------------------------------------------------
%%\part{''part''} 				-1 	not in letters
%%\chapter{''chapter''} 			0 	only books and reports
%%\section{''section''} 			1 	not in letters
%%\subsection{''subsection''} 		2 	not in letters
%%\subsubsection{''subsubsection''}	3 	not in letters
%%\paragraph{''paragraph''} 		4 	not in letters
%%\subparagraph{''subparagraph''}
%
%% CODEBLOCK 
%\begin{lstlisting}[frame=single]  % Start your code-block
%for i:=maxint to 0 do
%begin
%{ do nothing }
%end;
%Write('Case insensitive ');
%Write('Pascal keywords.');
%\end{lstlisting}
%
%
%% PSEUDOCODE % mirror.ctan.org/macros/latex/contrib/program/program-doc.pdf
%\begin{program}
%\mbox{A fast exponentiation procedure:}
%\BEGIN \\ %
%  \FOR i:=1 \TO 10 \STEP 1 \DO
%     |expt|(2,i); \\ |newline|() \OD %
%\rcomment{This text will be set flush to the right margin}
%\WHERE
%\PROC |expt|(x,n) \BODY
%          z:=1;
%          \DO \IF n=0 \THEN \EXIT \FI;
%             \DO \IF |odd|(n) \THEN \EXIT \FI;
%\COMMENT{This is a comment statement};
%                n:=n/2; x:=x*x \OD;
%             \{ n>0 \};
%             n:=n-1; z:=z*x \OD;
%          |print|(z) \ENDPROC
%\END
%\end{program}
%
%
%
%\captionsetup[table]{textfont=it,format=plain,justification=justified,
%  singlelinecheck=false,skip=0pt}
%
%% Start the table
%\begin{table}[htpb]
%\caption{The caption of the table.}
%\begin{tabular}{cccc}
%\toprule
%Column 1 & \multicolumn{3}{c}{Column 2} \\
%& Column 2a & Column 2b & Column2c \\
%\midrule
%1 & 1 & .67 & 5 \\
%1 & 2 & .32 & 2 \\
%1 & 3 & .01 & 4 \\
%\bottomrule
%\end{tabular}
%
%\bigskip
%\small\textit{Note}. The caption should now be correctly formatted.
%\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%OBSOLETE BUILDING BLOCKS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage

% Building Blocks
\newpage %----------------------------------------------------------------
% CODEFRAME BLOCK
\begin{mdframed}[style=Codeframe]
\textbf{THIS TEXT SHOULD BE MORE TIGHTLY FRAMED} \par
Now flowtext
\end{mdframed}

% CITATIONS
\cite{Elwert2013}  \\					% Full citation
\citeNP{Elwert2013} \\					% Full citation, no parenthesis
\cite[p.~12]{Elwert2013} \\				% Full citation with page reference
\citeA<e.g.,>[p.~11]{Elwert2013} \\ 			% Full citation of the author outside the parenthesis
\citeyear<e.g.,>[p.~11]{Elwert2013} \\		% Ciitation of the year and page only in the parenthesis
\citeyearNP<e.g.,>[p.~11]{Elwert2013} \\		% Citation of the year and page only, no parenthesis


% CODEBLOCK
\begin{figure}[H]
\begin{center}
\includegraphics[trim = 0 0 0 0, clip = true, width = 5cm]{thumbnail.png}
\caption{A picture}
\label{fig:ALabel}
\end{center}
\end{figure}

% CODEBLOCK DESCRIPTION
\begin{description}[itemsep = 0 pt, nolistsep, leftmargin = 0 cm]
\item [Age:] Participants age
\item [Position:] Preferred position 
\end{description}
% Hierarchy
%---------------------------------------------------------------------------
%\part{''part''} 				-1 	not in letters
%\chapter{''chapter''} 			0 	only books and reports
%\section{''section''} 			1 	not in letters
%\subsection{''subsection''} 		2 	not in letters
%\subsubsection{''subsubsection''}	3 	not in letters
%\paragraph{''paragraph''} 		4 	not in letters
%\subparagraph{''subparagraph''}

% CODEBLOCK 
\begin{lstlisting}[frame=single]  % Start your code-block
for i:=maxint to 0 do
begin
{ do nothing }
end;
Write('Case insensitive ');
Write('Pascal keywords.');
\end{lstlisting}


% PSEUDOCODE % mirror.ctan.org/macros/latex/contrib/program/program-doc.pdf
\begin{program}
\mbox{A fast exponentiation procedure:}
\BEGIN \\ %
  \FOR i:=1 \TO 10 \STEP 1 \DO
     |expt|(2,i); \\ |newline|() \OD %
\rcomment{This text will be set flush to the right margin}
\WHERE
\PROC |expt|(x,n) \BODY
          z:=1;
          \DO \IF n=0 \THEN \EXIT \FI;
             \DO \IF |odd|(n) \THEN \EXIT \FI;
\COMMENT{This is a comment statement};
                n:=n/2; x:=x*x \OD;
             \{ n>0 \};
             n:=n-1; z:=z*x \OD;
          |print|(z) \ENDPROC
\END
\end{program}



\captionsetup[table]{textfont=it,format=plain,justification=justified,
  singlelinecheck=false,skip=0pt}

% Start the table
\begin{table}[htpb]
\caption{The caption of the table.}
\begin{tabular}{cccc}
\toprule
Column 1 & \multicolumn{3}{c}{Column 2} \\
& Column 2a & Column 2b & Column2c \\
\midrule
1 & 1 & .67 & 5 \\
1 & 2 & .32 & 2 \\
1 & 3 & .01 & 4 \\
\bottomrule
\end{tabular}

\bigskip
\small\textit{Note}. The caption should now be correctly formatted.
\end{table}


%% Statistical distributions
$X\sim N(\mu,\,\sigma^{2})$



% References 								
%---------------------------------------------------------------------------
\newpage
\bibliographystyle{apacite}				% Use APA style for references
\bibliography{02_Bibliography/Bibliography}			% Inserts a Reference list with title


\end{document}
